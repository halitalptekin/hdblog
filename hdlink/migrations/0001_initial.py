# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Link'
        db.create_table('hdlink_link', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('link', self.gf('django.db.models.fields.URLField')(max_length=200)),
            ('hits', self.gf('django.db.models.fields.IntegerField')(default=0)),
        ))
        db.send_create_signal('hdlink', ['Link'])

        # Adding model 'HitsDatePoint'
        db.create_table('hdlink_hitsdatepoint', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('day', self.gf('django.db.models.fields.DateField')(auto_now=True, db_index=True, blank=True)),
            ('hits', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('link', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['hdlink.Link'])),
        ))
        db.send_create_signal('hdlink', ['HitsDatePoint'])

        # Adding unique constraint on 'HitsDatePoint', fields ['day', 'link']
        db.create_unique('hdlink_hitsdatepoint', ['day', 'link_id'])


    def backwards(self, orm):
        # Removing unique constraint on 'HitsDatePoint', fields ['day', 'link']
        db.delete_unique('hdlink_hitsdatepoint', ['day', 'link_id'])

        # Deleting model 'Link'
        db.delete_table('hdlink_link')

        # Deleting model 'HitsDatePoint'
        db.delete_table('hdlink_hitsdatepoint')


    models = {
        'hdlink.hitsdatepoint': {
            'Meta': {'unique_together': "(('day', 'link'),)", 'object_name': 'HitsDatePoint'},
            'day': ('django.db.models.fields.DateField', [], {'auto_now': 'True', 'db_index': 'True', 'blank': 'True'}),
            'hits': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'link': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['hdlink.Link']"})
        },
        'hdlink.link': {
            'Meta': {'object_name': 'Link'},
            'hits': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'link': ('django.db.models.fields.URLField', [], {'max_length': '200'})
        }
    }

    complete_apps = ['hdlink']