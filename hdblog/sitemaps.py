from hdyazi.models import Yazilar, Kategoriler
from hdmakale.models import Makaleler, mKategoriler
from hdsayfalar.models import Sayfalar
from hddosya.models import Dosyalar
from hdduyuru.models import Duyurular
from hdreklam.models import TanitimYazisi

from django.contrib.sitemaps import FlatPageSitemap, GenericSitemap
from django.conf.urls.defaults import patterns, url

yazilar_dict = {
    'queryset': Yazilar.objects.order_by("-olusturulma").filter(anasayfa_sabit=True),
    'date_field': 'olusturulma',
}
kategoriler_dict = {
    'queryset': Kategoriler.objects.all(),
}
makaleler_dict = {
    'queryset': Makaleler.objects.order_by("-olusturulma").filter(anasayfa_sabit=True),
    'date_field': 'olusturulma',
}
mkategoriler_dict = {
    'queryset': mKategoriler.objects.all(),
}
sayfalar_dict = {
    'queryset': Sayfalar.objects.all(),
    'date_field': 'olusturulma',
}
dosyalar_dict = {
              'queryset': Dosyalar.objects.filter(gizli=False).order_by("-olusturulma"),
              'date_field': 'olusturulma',
}
duyurular_dict = {
              'queryset': Duyurular.objects.all(),
              'date_field': 'olusturulma',
}
tanitimyazisi_dict = {
              'queryset': TanitimYazisi.objects.filter(yayindami=True).all(),
              'date_field': 'olusturulma',

}
sitemaps = {
    'yazilar': GenericSitemap(yazilar_dict, priority=0.5),
    'kategoriler': GenericSitemap(kategoriler_dict, priority=1),
	'makaleler': GenericSitemap(makaleler_dict, priority=0.5),
	'makale-kategoriler': GenericSitemap(mkategoriler_dict, priority=1),
    'sayfalar': GenericSitemap(sayfalar_dict, priority=0.5),
	'dosyalar': GenericSitemap(dosyalar_dict, priority=0.5),
    'duyurular': GenericSitemap(duyurular_dict, priority=0.5),
	'tanitim-yazisi': GenericSitemap(tanitimyazisi_dict, priority=0.5),
}


SITEMAPS_URLS = patterns('django.contrib.sitemaps.views',
    url(r'^sitemap\.xml$', 'index', {'sitemaps': sitemaps}),
    url(r'^sitemap-(?P<section>.+)\.xml$', 'sitemap', {'sitemaps': sitemaps}),
)