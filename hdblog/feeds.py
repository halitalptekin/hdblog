from django.contrib.syndication.views import Feed
from django.conf.urls.defaults import patterns, url

from django.shortcuts import get_object_or_404

from hdyazi.models import Yazilar
from hdmakale.models import Makaleler
from hddosya.models import Dosyalar
from hdduyuru.models import Duyurular


class SonMakaleler(Feed):
    
    baslik = "HalitAlptekin.Com - Son Makaleler"
    link = "/"
    description = "Programming and Life Blog Of Halit Alptekin. Karadeniz Technical University Computer Engineering Student."


    def items(self):
        return Makaleler.objects.order_by("-olusturulma").filter(anasayfa_sabit=True)[:5]


    def item_baslik(self, item):
        return item.sef_baslik


    def item_description(self, item):
        return item.aciklama_sef

    
    def item_pubdate(self, item):
        return item.olusturulma


class SonYazilar(Feed):
    
    baslik = "HalitAlptekin.Com - Son Blog Yazilari"
    link = "/"
    description = "Programming and Life Blog Of Halit Alptekin. Karadeniz Technical University Computer Engineering Student."


    def items(self):
        return Yazilar.objects.order_by("-olusturulma").filter(anasayfa_sabit=True)[:5]


    def item_baslik(self, item):
        return item.sef_baslik


    def item_description(self, item):
        return item.aciklama_sef

    
    def item_pubdate(self, item):
        return item.olusturulma

class SonDuyurular(Feed):
    
    baslik = "HalitAlptekin.Com - Son Duyurular"
    link = "/"
    description = "Programming and Life Blog Of Halit Alptekin. Karadeniz Technical University Computer Engineering Student."


    def items(self):
        return Duyurular.objects.order_by("-olusturulma")[:5]


    def item_baslik(self, item):
        return item.sef_baslik


    def item_description(self, item):
        return item.aciklama_sef

    
    def item_pubdate(self, item):
        return item.olusturulma
class SonDosyalar(Feed):
    
    baslik = "HalitAlptekin.Com - Son Dosyalar"
    link = "/"
    description = "Programming and Life Blog Of Halit Alptekin. Karadeniz Technical University Computer Engineering Student."


    def items(self):
        return Dosyalar.objects.order_by("-olusturulma").filter(gizli=False)[:5]


    def item_baslik(self, item):
        return item.sef_baslik


    def item_description(self, item):
        return item.aciklama_sef

    
    def item_pubdate(self, item):
        return item.olusturulma

RSS_URLS = patterns('',
                    url(r'^rss/$', SonYazilar()),
                    url(r'^rss/makaleler/$', SonMakaleler()),
                    url(r'^rss/yazilar/$', SonYazilar()),
                    url(r'^rss/duyurular/$', SonDuyurular()),
					url(r'^rss/dosyalar/$', SonDosyalar()),
)