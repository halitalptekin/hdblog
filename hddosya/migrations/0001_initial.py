# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Dosyalar'
        db.create_table('hddosya_dosyalar', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('baslik', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('sef_baslik', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('slug', self.gf('django.db.models.fields.SlugField')(max_length=255)),
            ('aciklama', self.gf('django.db.models.fields.TextField')(max_length=500)),
            ('aciklama_sef', self.gf('django.db.models.fields.TextField')(max_length=500, blank=True)),
            ('dosya', self.gf('django.db.models.fields.files.FileField')(max_length=100)),
            ('dosya_turu', self.gf('django.db.models.fields.CharField')(max_length=10, blank=True)),
            ('dosya_resim', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('dosya_kaynak', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('gizli', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('olusturulma', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('degistirilme', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
        ))
        db.send_create_signal('hddosya', ['Dosyalar'])


    def backwards(self, orm):
        # Deleting model 'Dosyalar'
        db.delete_table('hddosya_dosyalar')


    models = {
        'hddosya.dosyalar': {
            'Meta': {'object_name': 'Dosyalar'},
            'aciklama': ('django.db.models.fields.TextField', [], {'max_length': '500'}),
            'aciklama_sef': ('django.db.models.fields.TextField', [], {'max_length': '500', 'blank': 'True'}),
            'baslik': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'degistirilme': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'dosya': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'dosya_kaynak': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'dosya_resim': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'dosya_turu': ('django.db.models.fields.CharField', [], {'max_length': '10', 'blank': 'True'}),
            'gizli': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'olusturulma': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'sef_baslik': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['hddosya']